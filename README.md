# README #

### UI Test ###
This is a test project for UI Programmer

### Tools ###
This project was created and built using newest Unity 2017.2.0f3 (64-bit) version.
Visual Studio 2017 with Resharper tool was used for the coding part.
Trello boards were used for task management. (https://trello.com/)

### How To Run the Project ###
1. Open project folder in Unity
2. Select Windows platform (64 bit)
3. Press Build & Run

### Important Notes ###
- All the UI was created using only UnityUI system and it's components.
- Project contains two seperate scenes to showcase the usage of basic menu elements in two different menus
- Same DetailsPanel was used for both menus, mainly because it displays the same functionality that was needed in both menus.
- CSV format was used as a default data format for reading content structures. They can be found under Assets/CSV Files/ folder
- For Two attached example menus there are seperate scenes for the sake of presentation.
- TutorialMenuMainView.cs and GlossaryMenuMainView.cs are the main entry views for the two example menus
- DropdownMenuItemView.cs is the class for all the dropdown menu views, which spawns additional content items during initialization
- ContentItemView.cs is the main view class for the basic menu item that can be selected.
- ContentData.cs is the file where the CSV file is read and loaded into proper data structure. This can be changed for other formats like JSON or XML with just adding additional Load methods.
- As much as possible all contents are event driven, so they can be reused in other menus without any references to other objects - thus less spaghetti
- Search and Filter are done using simplest of methods (Linq), and display of objects are controlled by turning off/on the gameobjects itself. Those methods can be more complex with usage of some better algorithms for search/sort, but for the sake of this test the simplest aproach was chosen. 

### Rough estimate of working hours spent on the whole project ###
- 20h(coding, creating menus etc.) 
- ~5h (dealing with unity bugs/updating tools and setting up project/eating and drinking) 

### Project Owner ###
All rights reserved by Adam Pach