﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UITest {
    
    public class TutorialMenuMainView : MonoBehaviour {

        [SerializeField] private GameObject contentItemPrefab;
        [SerializeField] private Transform contentRoot;
        [SerializeField] private TextAsset csvFile;
        [SerializeField] private DetailsView detailsPanel;
        [SerializeField] private Text sortingText;
        private readonly List < ContentItemView > contentItems = new List < ContentItemView >();
        private ContentData menuItems;
        private SortType currentSortType;

        private void Awake() {
            menuItems = new ContentData();
            menuItems.Load( csvFile );

            if ( detailsPanel == null ) {
                detailsPanel = FindObjectOfType<DetailsView>();
            }
        }
        private void Start() {
            List < ContentItem > items = menuItems.ItemsList;
            foreach ( ContentItem item in items ) {
                var view = Instantiate( contentItemPrefab, contentRoot ).GetComponent<ContentItemView>( );
                view.Initialize( item );
                contentItems.Add( view );
            }
            ContentItemView.OnClicked += ShowDetailsPanel;
            detailsPanel.OnRemoved += DetailsPanelOnRemoved;
            //le'ts select the first object
            EventSystem.current.SetSelectedGameObject( contentRoot.GetComponentInChildren<ContentItemView>( true ).gameObject );
        }
        private void DetailsPanelOnRemoved( ContentItemView arg0 ) {
            var temp = contentRoot.GetComponentsInChildren < ContentItemView >( true );
            GameObject obj = temp.First( view => view != arg0 ).gameObject;
            EventSystem.current.SetSelectedGameObject( obj );
            Destroy( arg0.gameObject );
        }
        private void AddItem( ContentItem item ) {
            var view = Instantiate( contentItemPrefab, contentRoot ).GetComponent<ContentItemView>( );
            view.Initialize( item );
            contentItems.Add( view );
        }

        public void AddRandomItem() {
            AddItem( menuItems.ItemsList[ UnityEngine.Random.Range( 0 , menuItems.ItemsList.Count ) ] );
        }

        void ShowDetailsPanel( ContentItemView item ) {
            detailsPanel.Display( item );
        }

        public void OnSearchCommand( string search ) {
            FilterByString( search.ToUpper() );
        }

        public void OnSearchEndCommand( string search ) {
            FilterByString( search.ToUpper() );
        }

        private void FilterByString( string filter ) {
            foreach ( var view in contentItems ) {
                view.gameObject.SetActive( view.ItemData.Title.StartsWith( filter ) );
            }
        }

        public void OnSortCommand() {
            SortItems( ++currentSortType );
        }

        private void SortItems( SortType type ) {
            if ( type > SortType.NewestToOldest ) {
                type = currentSortType = SortType.Alphabetical;
            }
            switch ( type ) {
                case SortType.Alphabetical:
                    sortingText.text = "Sort: Alphabetical";
                    ContentItemView[] lol = contentRoot
                    .transform.GetComponentsInChildren< ContentItemView >().OrderBy( view => view.ItemData.Title ).ToArray();
                    for ( int i = 0 ; i < lol.Length ; i++ ) {
                        lol[ i ].transform.SetSiblingIndex( i );
                    }
                    break;
                case SortType.NewestToOldest:
                default:
                    sortingText.text = "Sort: Newest To Oldest";
                    ContentItemView[] lol1 = contentRoot
                    .transform.GetComponentsInChildren < ContentItemView >().OrderBy( view => view.ItemId ).ToArray();
                    for ( int i = 0 ; i < lol1.Length ; i++ ) {
                        lol1[ i ].transform.SetSiblingIndex( i );
                    }
                    break;

            }
        }
        private void OnDestroy() {
            ContentItemView.OnClicked -= ShowDetailsPanel;
        }
    }
}

