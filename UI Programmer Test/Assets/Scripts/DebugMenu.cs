﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class DebugMenu : MonoBehaviour {

    public void ShowGlossaryScene() {
        SceneManager.LoadScene( 0 );
    }

    public void ShowtutorialScene() {
        SceneManager.LoadScene( 1 );
    }
}
