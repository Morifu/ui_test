﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UITest {
    public class DetailsView : MonoBehaviour {

        public event UnityAction < ContentItemView > OnRemoved; 

        [SerializeField] private Image icon;
        [SerializeField] private Text title;
        [SerializeField] private Text description;
        [SerializeField] private Button removeButton;
        private ContentItemView current;

        public void Display( ContentItemView item ) {
            current = item;
            icon.sprite = ContentData.Icons.Find( sprite => sprite.name == item.ItemData.IconName );
            title.text = item.ItemData.Title;
            description.text = item.ItemData.Description;
            if ( !gameObject.activeSelf ) {
                gameObject.SetActive( true );
            }
            EventSystem.current.SetSelectedGameObject( removeButton.gameObject );
        }

        public void OnRemoveCommand() {
            current.Remove();
            gameObject.SetActive( false );
            if ( OnRemoved != null ) {
                OnRemoved( current );
            }
        }
        
        public void OnCloseCommand() {
            gameObject.SetActive( false );
            EventSystem.current.SetSelectedGameObject( current.gameObject );
        }
    }

}