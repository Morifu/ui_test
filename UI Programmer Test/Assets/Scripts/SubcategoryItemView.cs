﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

namespace UITest {
    public class SubcategoryItemView : MonoBehaviour {
        [SerializeField] private Text title;
        [SerializeField] private Transform contentRoot;
        private readonly List <ContentItemView> itemViews = new List < ContentItemView >();
        public int SubCategoryId { get; private set; }

        public string Title {
            get { return title.text; }
            set { title.text = value; }
        }

        public Transform ContentRoot {
            get { return contentRoot; }
        }

        public bool IsEmptyOrChildsAreInvisible {
            get { return itemViews.Count <= 0 || itemViews.TrueForAll( view => !view.gameObject.activeSelf ); }
        }

        public void Initialize( int subcategoryId, string subCatTitle ) {
            title.text = subCatTitle;
            SubCategoryId = subcategoryId;
        }

        public void AddItem( ContentItemView view ) {
            if ( itemViews.Contains( view ) )
                return;
            itemViews.Add( view );
            view.transform.SetParent( contentRoot );
            view.OnRemoved += RemoveItem;
            if ( !gameObject.activeSelf ) {
                gameObject.SetActive( true );
            }
        }
        public void RemoveItem( ContentItemView view ) {
            if ( itemViews.Contains( view ) ) {
                itemViews.Remove( view );
                if ( itemViews.Count <= 0 ) {
                    gameObject.SetActive( false );
                }
            }

        }

        public void SortBy( SortType type ) {
            switch ( type ) {
                case SortType.Alphabetical:
                    ContentItemView[] lol = contentRoot
                    .transform.GetComponentsInChildren< ContentItemView >().OrderBy( view => view.ItemData.Title ).ToArray();
                    for ( int i = 0 ; i < lol.Length ; i++ ) {
                        lol[ i ].transform.SetSiblingIndex( i );
                    }
                    break;
                case SortType.NewestToOldest:
                default:
                    ContentItemView[] lol1 = contentRoot
                    .transform.GetComponentsInChildren < ContentItemView >().OrderBy( view => view.ItemId ).ToArray();
                    for ( int i = 0 ; i < lol1.Length ; i++ ) {
                        lol1[ i ].transform.SetSiblingIndex( i );
                    }
                    break;

            }
        }
    }
}