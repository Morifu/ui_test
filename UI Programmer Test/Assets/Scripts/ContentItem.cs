﻿namespace UITest {
    public class ContentItem {
        public string CategoryId;
        public string CategoryName;
        public string Subcategory;
        public string Title;
        public string Description;
        public string IconName;
    }
}