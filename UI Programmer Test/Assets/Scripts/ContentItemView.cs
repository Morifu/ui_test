﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace UITest {

    public class ContentItemView : MonoBehaviour {

        public static event UnityAction<ContentItemView> OnClicked;
        public event UnityAction<ContentItemView> OnRemoved;
        [SerializeField] private Image icon;
        [SerializeField] private Text title;
        [SerializeField] private GameObject newMarker;
        public ContentItem ItemData { get; private set; }
        private static int idCount = 0;
        private bool isNew = true;
        public int ItemId { get; private set; }
        private void Awake() { GetComponent < Button >().onClick.AddListener( OnClick ); }

        public void Initialize( ContentItem data ) {
            ItemId = idCount = idCount + 1;
            ItemData = data;
            icon.sprite = ContentData.Icons.Find( sprite => sprite.name == data.IconName );
            title.text = ItemData.Title;
        }

        public void Remove() {
            if ( OnRemoved != null ) {
                OnRemoved(this);
            }
        }

        private void OnClick() {
            if ( isNew ) {
                isNew = false;
                newMarker.SetActive( false );
            }
            if ( OnClicked != null ) {
                OnClicked( this );
            }
        }

    }
}