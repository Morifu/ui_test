﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UITest {

    public class DropdownMenuItemView : MonoBehaviour {

        [SerializeField] private GameObject subcategoryItemPrefab;
        [SerializeField] private GameObject contentItemPrefab;
        [SerializeField] private Transform contentRoot;
        [SerializeField] private Image expandArrow;
        [SerializeField] private Text title;
        public int CategoryId { get; private set; }
        public string CategoryName { get; private set; }
        //private List < ContentItem > items;
        private bool isInitialized;
        private bool showSubcategories = true;
        // the menu item to subcategory pairing, mainly for reparenting
        private readonly Dictionary < ContentItemView, SubcategoryItemView > subcategoryPairings = new Dictionary < ContentItemView, SubcategoryItemView >();
        private readonly Dictionary < string, SubcategoryItemView > subCategoriesViews = new Dictionary < string, SubcategoryItemView >();


        public void Initialize( int id , string categoryName , List<ContentItem> categoryItems ) {
            if ( isInitialized ) {
                return;
            }
            //items = categoryItems;
            CategoryId = id;
            title.text = CategoryName = categoryName;
            foreach ( ContentItem item in categoryItems ) {
                string subCat = item.Subcategory;

                if ( !subCategoriesViews.ContainsKey( subCat ) ) {
                    SubcategoryItemView subCatTrans = Instantiate( subcategoryItemPrefab, contentRoot ).GetComponent<SubcategoryItemView>();
                    subCatTrans.Initialize( subCategoriesViews.Count , subCat );
                    subCategoriesViews.Add( subCat , subCatTrans );
                }
                ContentItemView view = Instantiate( contentItemPrefab, subCategoriesViews[subCat].transform ).GetComponent<ContentItemView>();
                view.Initialize( item );
                view.OnRemoved += RemoveItem;
                subCategoriesViews[ subCat ].AddItem( view );
                subcategoryPairings.Add( view , subCategoriesViews[ subCat ] );
            }

            isInitialized = true;
        }

        public void AddItem( ContentItem item ) {
            //if ( items.Contains( item ) ) {
            //    return;
            //}
            string subCat = item.Subcategory;

            if ( !subCategoriesViews.ContainsKey( subCat ) || subCategoriesViews.ContainsKey( subCat ) && subCategoriesViews[ subCat ] == null ) {
                SubcategoryItemView subCatTrans = Instantiate( subcategoryItemPrefab, contentRoot ).GetComponent<SubcategoryItemView>();
                subCatTrans.Initialize( subCategoriesViews.Count , subCat );
                subCategoriesViews.Add( subCat , subCatTrans );
            }
            ContentItemView view = Instantiate( contentItemPrefab, subCategoriesViews[subCat].transform ).GetComponent<ContentItemView>();
            view.Initialize( item );
            view.OnRemoved += RemoveItem;
            subCategoriesViews[ subCat ].AddItem( view );
            subcategoryPairings.Add( view , subCategoriesViews[ subCat ] );
            //items.Add( item );
        }
        private void RemoveItem( ContentItemView item ) {
            subcategoryPairings[ item ].RemoveItem( item );
            subcategoryPairings.Remove( item );
            //items.Remove( item.ItemData );
            Destroy( item.gameObject );
        }

        public void ToggleView( bool isExpanded ) {
            expandArrow.rectTransform.Rotate( 0f , 0f , 180f );
            contentRoot.gameObject.SetActive( isExpanded );
        }
        public void ToggleShowSubcategories( bool show ) {
            showSubcategories = show;
            foreach ( var pair in subcategoryPairings ) {
                pair.Key.transform.SetParent( show ? pair.Value.ContentRoot : contentRoot );
                pair.Value.gameObject.SetActive( show && !pair.Value.IsEmptyOrChildsAreInvisible );
            }
        }

        public bool FilterBySTring( string filter ) {
            bool retVal = string.IsNullOrEmpty( filter );
            foreach ( var pairing in subcategoryPairings ) {
                if ( string.IsNullOrEmpty( filter ) || pairing.Key.ItemData.Title.ToUpper().StartsWith( filter ) ) {
                    retVal = true;
                    pairing.Key.gameObject.SetActive( true );
                } else {
                    pairing.Key.gameObject.SetActive( false );
                }
                pairing.Value.gameObject.SetActive( !pairing.Value.IsEmptyOrChildsAreInvisible );
            }
            return retVal;
        }

        public void SortBy( SortType type ) {
            switch ( type ) {
                case SortType.Alphabetical:
                    if ( showSubcategories ) {
                        SubcategoryItemView[] lol = contentRoot
                        .transform.GetComponentsInChildren< SubcategoryItemView >().OrderBy( view => view.Title ).ToArray();
                        for ( int i = 0 ; i < lol.Length ; i++ ) {
                            lol[ i ].transform.SetSiblingIndex( i );
                            lol[ i ].SortBy( type );
                        }
                    } else {
                        ContentItemView[] civiews = contentRoot.transform.GetComponentsInChildren < ContentItemView >();
                        if ( civiews != null ) {
                            civiews = civiews.OrderBy( view => view.ItemData.Title ).ToArray();
                            for ( int i = 0 ; i < civiews.Length ; i++ ) {
                                civiews[ i ].transform.SetSiblingIndex( i );
                            }
                        }
                    }
                    break;
                case SortType.NewestToOldest:
                default:
                    if ( showSubcategories ) {
                        SubcategoryItemView[] lol1 = contentRoot
                        .transform.GetComponentsInChildren < SubcategoryItemView >().OrderBy( view => view.SubCategoryId ).ToArray();
                        for ( int i = 0 ; i < lol1.Length ; i++ ) {
                            lol1[ i ].transform.SetSiblingIndex( i );
                            lol1[ i ].SortBy( type );
                        }
                    } else  {
                        ContentItemView[] civiews = contentRoot.transform.GetComponentsInChildren < ContentItemView >(true);
                        if ( civiews != null ) {
                            civiews = civiews.OrderBy( view => view.ItemId ).ToArray();
                            for ( int i = 0 ; i < civiews.Length ; i++ ) {
                                civiews[ i ].transform.SetSiblingIndex( i );
                            }
                        }
                    }
                    break;

            }
        }
    }
}