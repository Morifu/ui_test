﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class AutoScroll : MonoBehaviour {

    [SerializeField] private ScrollRect scrollRect;
    [SerializeField] private RectTransform contentPanel;
    private GameObject currentSelection;

    private void Start() { StartCoroutine( CheckScroll() ); }

    private IEnumerator CheckScroll() {
        yield return new WaitForSeconds( 1f );
        while ( true ) {
            yield return new WaitForFixedUpdate();
            GameObject currsel = EventSystem.current.currentSelectedGameObject;
            if ( currsel == null || currentSelection == currsel ) {
                yield return new WaitForFixedUpdate();
                continue;
            }
            currentSelection = currsel;
            RectTransform target = currsel.GetComponent < RectTransform >();
            if ( !target.IsChildOf( contentPanel ) ) {
                yield return new WaitForFixedUpdate();
                continue;
            }
            var objPosition = (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
            var scrollHeight = scrollRect.GetComponent<RectTransform>().rect.height;
            var objHeight = target.rect.height;

            if ( objPosition.y + objHeight / 2 > scrollHeight / 2 ) {
                contentPanel.localPosition = new Vector2( contentPanel.localPosition.x ,
                                                          contentPanel.localPosition.y - objHeight );
            }

            if ( objPosition.y - objHeight / 2 < -scrollHeight / 2 ) {
                contentPanel.localPosition = new Vector2( contentPanel.localPosition.x ,
                                                          contentPanel.localPosition.y + objHeight );
            }
        }
    }
}