﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UITest {

    public enum SortType {
        Alphabetical = 0,
        NewestToOldest
    }
    public class GlossaryMenuMainView : MonoBehaviour {

        [SerializeField] private GameObject dropdownMenuPrefab;
        [SerializeField] private Transform contentRoot;
        [SerializeField] private TextAsset csvFile;
        [SerializeField] private DetailsView detailsPanel;
        [SerializeField] private Text sortingText;
        private readonly Dictionary < int, DropdownMenuItemView > contentItems = new Dictionary < int, DropdownMenuItemView >();
        private ContentData menuItems;
        private SortType currentSortType;

        private void Awake() {
            menuItems = new ContentData();
            menuItems.Load( csvFile );

            if ( detailsPanel == null ) {
                detailsPanel = FindObjectOfType<DetailsView>();
            }
        }
        private void Start() {
            List < ContentItem > items = menuItems.ItemsList;
            foreach ( ContentItem item in items ) {
                int categoryId;
                int.TryParse( item.CategoryId , out categoryId );
                if ( contentItems.ContainsKey( categoryId ) )
                    continue;
                var dropdown = Instantiate( dropdownMenuPrefab, contentRoot ).GetComponent<DropdownMenuItemView>( );
                dropdown.name = item.CategoryName;
                dropdown.Initialize( categoryId , item.CategoryName , items.FindAll( contentItem => contentItem.CategoryId == item.CategoryId ) );
                contentItems.Add( categoryId , dropdown );
            }
            ContentItemView.OnClicked += ShowDetailsPanel;
            detailsPanel.OnRemoved += DetailsPanelOnRemoved;
            //le'ts select the first object
            EventSystem.current.SetSelectedGameObject( contentRoot.GetComponentInChildren<ContentItemView>( true ).gameObject );
            //Canvas.ForceUpdateCanvases();
        }
        private void DetailsPanelOnRemoved( ContentItemView arg0 ) {
            var temp = contentRoot.GetComponentsInChildren < ContentItemView >( true );
            GameObject obj = temp.First( view => view != arg0 ).gameObject;
            EventSystem.current.SetSelectedGameObject( obj );
        }
        private void AddItem( ContentItem item ) {
            int categoryId;
            int.TryParse( item.CategoryId , out categoryId );
            if ( !contentItems.ContainsKey( categoryId ) ) {
                var dropdown = Instantiate( dropdownMenuPrefab, contentRoot ).GetComponent < DropdownMenuItemView >();
                dropdown.name = item.CategoryName;
                dropdown.Initialize( categoryId , item.CategoryName ,
                                     menuItems.ItemsList.FindAll( contentItem => contentItem.CategoryId == item.CategoryId ) );
                contentItems.Add( categoryId , dropdown );
            } else {
                contentItems[ categoryId ].AddItem( item );
            }
        }

        public void AddRandomItem() {
            AddItem( menuItems.ItemsList[ UnityEngine.Random.Range( 0 , menuItems.ItemsList.Count ) ] );
        }

        public void ShowSubcategories( bool show ) {
            foreach ( var item in contentItems.Values ) {
                item.ToggleShowSubcategories( show );
            }
            SortItems( currentSortType );
        }

        void ShowDetailsPanel( ContentItemView item ) {
            detailsPanel.Display( item );
        }

        public void OnSearchCommand( string search ) {
            FilterByString( search.ToUpper() );
        }

        public void OnSearchEndCommand( string search ) {
            FilterByString( search.ToUpper() );
        }

        private void FilterByString( string filter ) {
            foreach ( var view in contentItems.Values ) {
                view.gameObject.SetActive( view.FilterBySTring( filter ) );
            }
        }

        public void OnSortCommand() {
            SortItems( ++currentSortType );
        }

        private void SortItems( SortType type ) {
            if ( type > SortType.NewestToOldest ) {
                type = currentSortType = SortType.Alphabetical;
            }
            switch ( type ) {
                case SortType.Alphabetical:
                    sortingText.text = "Sort: Alphabetical";
                    DropdownMenuItemView[] lol = contentRoot
                    .transform.GetComponentsInChildren< DropdownMenuItemView >().OrderBy( view => view.CategoryName ).ToArray();
                    for ( int i = 0 ; i < lol.Length ; i++ ) {
                        lol[ i ].transform.SetSiblingIndex( i );
                        lol[i].SortBy( type );
                    }
                    break;
                case SortType.NewestToOldest:
                default:
                    sortingText.text = "Sort: Newest To Oldest";
                    DropdownMenuItemView[] lol1 = contentRoot
                    .transform.GetComponentsInChildren < DropdownMenuItemView >().OrderBy( view => view.CategoryId ).ToArray();
                    for ( int i = 0 ; i < lol1.Length ; i++ ) {
                        lol1[ i ].transform.SetSiblingIndex( i );
                        lol1[ i ].SortBy( type );
                    }
                    break;

            }
        }

        private void OnDestroy() {
            ContentItemView.OnClicked -= ShowDetailsPanel;
        }
    }
}

